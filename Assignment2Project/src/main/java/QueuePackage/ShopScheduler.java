package QueuePackage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ShopScheduler{
	public ArrayList<Queue> queues; 
	public SelectionPolicy selectionPolicy;
	public int numberOfQueues; 
	
	public ShopScheduler(int nrOfQueues){
		this.queues = new ArrayList<Queue>();
		this.selectionPolicy = SelectionPolicy.SHORTEST_TIME;
		this.numberOfQueues = nrOfQueues;
		
		this.createQueues();
	}
	  private void createQueues() {
		  int i=0;
		  while(i<numberOfQueues) {
			  Queue queue = new Queue(i+1);
			  this.queues.add(queue); 		
			  Thread thread = new Thread(queue);
			  thread.start();
			  i++;
		  }
	  }
	
	public int getNumberOfQueues(){
		return this.numberOfQueues;
	}
	
	public List<Queue> getQueues(){
		return Collections.unmodifiableList(queues);//read only list
	}
	
	
}