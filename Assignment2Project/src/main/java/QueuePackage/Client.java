package QueuePackage;

public class Client implements Comparable<Client>, Runnable {
	private int id;
	private int arrivalTime;
	private int serviceTime;
	
	public Client(int id, int arrivalTime, int serviceTime){
		this.id = id;
		this.arrivalTime = arrivalTime;
		this.serviceTime = serviceTime;
	}

	public void run() {
		try {
			Thread.sleep(serviceTime);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}

	
	 public int compareTo(Client c) {
		 return this.arrivalTime-c.arrivalTime;
	 }

	public int getArrivalTime() {
		return this.arrivalTime;
	}
	
	public int getId(){
		return this.id;
	}
	
	public int getServiceTime(){
		return this.serviceTime;
	}

	public String convertClientToString(){
		return "("+Integer.toString(id)+","+Integer.toString(arrivalTime)+","+Integer.toString(serviceTime)+");";
	}
	
}