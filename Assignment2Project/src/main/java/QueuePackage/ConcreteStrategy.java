package QueuePackage;

import java.util.List;

public class ConcreteStrategy implements Strategy{
	
	public void addClientStrategy(List<Queue> queues, Client c){
		Queue minimumQ = queues.get(0);
		int time = minimumQ.getWaitingTime().get();
		for(Queue queue: queues){
			int time2 = queue.getWaitingTime().get();
			if( time>time2){
				minimumQ = queue;
				time = time2;
			}
		}		
		minimumQ.addClient(c);
	}

}
