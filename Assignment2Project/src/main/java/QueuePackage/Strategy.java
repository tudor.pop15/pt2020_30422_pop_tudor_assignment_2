package QueuePackage;

import java.util.List;

public interface Strategy {
	public void addClientStrategy(List<Queue> q, Client c);
}
