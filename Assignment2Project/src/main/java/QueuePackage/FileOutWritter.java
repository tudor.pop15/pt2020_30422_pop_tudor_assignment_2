package QueuePackage;

import java.io.FileWriter;
import java.io.IOException;

public class FileOutWritter {
	
	public ClientGen generator;
	public String outputFile;
	public FileWriter fileWriter;
	
	public FileOutWritter(ClientGen generator, String path) throws IOException{
		this.generator = generator;
		this.outputFile = path;
		this.fileWriter = new FileWriter(path);
	}

	private String waitingClientsConvert(){
		String c = new String("");
		for(Client client:generator.getClients()){
			c += client.convertClientToString();
		}
		return c;
	}
	
	public void writeState(int time) throws IOException{
		String s = new String("");
		s += "Time " + time + "\n";
		s += "Waiting clients: " + this.waitingClientsConvert() + "\n";
		this.fileWriter.append(s);
		System.out.print(s);
		for(Queue queue: generator.getShopScheduler().getQueues()){
			this.fileWriter.append(queue.convertQueueToString());
			System.out.print(queue.convertQueueToString());
		}
		this.fileWriter.append("\n");
		this.fileWriter.append("the average waiting time is : "+ generator.averageTime+"\n\n");
		System.out.println("the average waiting time is : " + generator.averageTime+"\n");
		this.fileWriter.flush();
	}
	
}