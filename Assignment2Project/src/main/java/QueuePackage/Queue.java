package QueuePackage;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Queue implements Runnable{
	private int index;
	private BlockingQueue<Client> clients;
	private AtomicInteger waitingTime;
	
	public Queue(int index){
		this.index = index;
		this.clients = new LinkedBlockingQueue<Client>();
		this.waitingTime = new AtomicInteger(0);	
	}
	
	public AtomicInteger getWaitingTime(){
		return this.waitingTime;
	}
	
    public void run() {
    	while(ClientGen.isRunning == 1){
			Client client = clients.peek();
			if(client!=null){			
				try {
					Thread.sleep(client.getServiceTime()*1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			this.waitingTime.addAndGet(-1*client.getServiceTime());
			this.clients.remove();
			}
		}

    }
    
    private void addWaitTime(Client c){
		this.waitingTime.addAndGet(c.getServiceTime());
	}
    
	public void addClient(Client c){
		this.clients.add(c);
		this.addWaitTime(c);
	}
	
	public String convertQueueToString(){
		String output = new String("");
		output += "Queue " + Integer.toString(this.index)+" : ";
		if(this.clients.size() == 0){
			output += "closed\n";
		}
		else{
			for(Client client:this.clients){
				output += client.convertClientToString();
			}
			output += "\n";
		}
		
		return output;
	}
	
}