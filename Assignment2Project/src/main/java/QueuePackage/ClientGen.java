package QueuePackage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;

public class ClientGen implements Runnable {
	private ShopScheduler scheduler;
	private ArrayList<Client> clients;
	private FileOutWritter writter;
	private Random random;
	private int timeLimit; 
	private int numberOfClients ; 
	private int numberOfQueues ; 
	int minArrivalTime ;  
	int maxArrivalTime ; 
	private int minServiceTime ; 
	private int maxServiceTime ; 
	public float averageTime=0;
	int totalServiceTime=0;
	public ClientGen(String input, String output) throws IOException{
		File inputFile= new File(input);
    	Scanner s = new Scanner(inputFile);
    	numberOfClients=s.nextInt();
    	numberOfQueues=s.nextInt();
    	timeLimit=s.nextInt();
    	minArrivalTime=s.nextInt();
    	maxArrivalTime=s.nextInt();
    	minServiceTime=s.nextInt();
    	maxServiceTime=s.nextInt();
    	s.close();
    	
    	this.random=new Random();
    	this.scheduler = new ShopScheduler(numberOfQueues);
    	this.clients = generateClients(numberOfClients,minArrivalTime,maxArrivalTime,minServiceTime,maxServiceTime);
		this.writter = new FileOutWritter(this, output);	
	}
	
	public ArrayList<Client> generateClients(int numberOfClients, int minArrivalTime, int maxArrivalTime, 
			                                 int minServiceTime, int maxServiceTime) {
		
		ArrayList<Client> clientsList = new ArrayList<Client>();
		for(int i=0; i<numberOfClients; i++) {
			int newServiceTime=this.random.nextInt((maxServiceTime-minServiceTime)+1)+minServiceTime;
			Client c = new Client(i,this.random.nextInt(( maxArrivalTime-minArrivalTime)+1)+minArrivalTime,newServiceTime);
			totalServiceTime=totalServiceTime+newServiceTime;
			clientsList.add(c);			
		}
		averageTime=(float)totalServiceTime/numberOfClients;
		Collections.sort(clientsList);
		return clientsList;
	}
    
	public static int isRunning = 1;
	
	public void run() {
		int time = 0;
		ConcreteStrategy dispatch = new ConcreteStrategy();
		while(time<this.timeLimit){	
			Iterator<Client> iter = clients.iterator();
			ArrayList<Client> removableClients = new ArrayList<Client>();	
			while(iter.hasNext()==true){
				Client c = iter.next();
				if(c.getArrivalTime() <= time){
					dispatch.addClientStrategy(scheduler.getQueues(), c);//inserts the client in the best queue
					removableClients.add(c);
				}
			}
			
			this.clients.removeAll(removableClients);
			try {
				writter.writeState(time);
			} catch (IOException e1) {
				System.out.println("Write error");
			}
			time++;	
			try {
				Thread.sleep(1000); 
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			} 
		}
		isRunning = 0;
		try {
			this.writter.fileWriter.close();
		} catch (IOException e) {
			System.out.println("Close error");
		}
	}
		
	public ArrayList<Client> getClients(){
		return this.clients;
	}
	
	public ShopScheduler getShopScheduler(){
		return this.scheduler;
	}

}
